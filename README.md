# Quilt Demo

A proof-of-concept that shows how to build a SQLite database, a
[Whoosh](https://whoosh.readthedocs.io/) search index for said database, and how
to access both of them after retrieving the data via
[quilt](https://quiltdata.com/).

## Usage

* The script `compound.py` was run for you, so a quilt package
  `midnighter/democache` already exists. You can reference it to look at the
  database setup.
* Run the `demo.py` script which will download the quilt data package, open the
  SQLite database and the Whoosh index and search both of them.

## Copyright

* Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
* Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
  Technical University of Denmark
* Free software distributed under the MIT license.

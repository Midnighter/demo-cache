# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging
from os.path import join
from tempfile import mkdtemp

import quilt
from sqlalchemy import create_engine
from whoosh import index
from whoosh.qparser import FuzzyTermPlugin, QueryParser

from compound import Compound, Session


logger = logging.getLogger()


def search(
    query: str, index: index, session, page: int = 1, page_size: int = 10
):
    """
    Parse the given query and return the compounds found.

    Parameters
    ----------
    query : str
        The desired search term(s).
    index : whoosh.index
        An open Whoosh search index.
    session : sqlalchemy.orm.session.Session
        An open session on the compound cache.
    page : int
        The desired page (default first).
    page_size : int
        The number of compounds per page.

    Returns
    -------
    list
        A list of compounds from the specified search result page.

    """
    parser = QueryParser("name", schema=index.schema)
    parser.add_plugin(FuzzyTermPlugin())
    parsed = parser.parse(query)
    with index.searcher() as searcher:
        results = (
            session.query(Compound)
            .filter(
                Compound.id.in_(
                    [
                        r["index"]
                        for r in searcher.search_page(
                            parsed, page, pagelen=page_size
                        )
                    ]
                )
            )
            .all()
        )
    return results


def main():
    quilt.install("midnighter/democache", force=True)
    location = str(mkdtemp())
    quilt._DEV_MODE = True
    quilt.export("midnighter/democache", location, force=True, symlinks=True)
    quilt._DEV_MODE = None

    # Ensure SQLite database integrity.
    engine = create_engine(f"sqlite:///{join(location, 'compounds.sqlite')}")
    session = Session(bind=engine)
    assert session.query(Compound).count() == 2

    # Test the Whoosh seearch index.
    idx = index.open_dir(join(location, "index"), readonly=True)
    results = search("pentane", idx, session)
    logger.info("Found %r", results)
    assert len(results) == 1
    assert results[0].mnx_id == "MNXM504217"
    results = search("methyldihyd", idx, session)
    logger.info("Found %r", results)
    assert len(results) == 1
    assert results[0].mnx_id == "MNXM39260"


if __name__ == "__main__":
    logging.basicConfig(level="INFO", format="%(message)s")
    main()

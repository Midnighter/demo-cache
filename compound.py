# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging
import os
from os.path import join, dirname
from shutil import rmtree

import quilt
import whoosh.index as search_index
from sqlalchemy import Column, Integer, String, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from whoosh.fields import NGRAM, STORED, SchemaClass


logger = logging.getLogger()


Base = declarative_base()
Session = sessionmaker()


class Compound(Base):
    """
    Model a chemical compound in the context of component contribution.

    Attributes
    ----------
    id : int
        The primary key in the table.
    mnx_id :
        The MetaNetX serial number of this metabolite.

    """

    __tablename__ = "compounds"

    # SQLAlchemy column descriptors.
    id: int = Column(Integer, primary_key=True, autoincrement=True)
    mnx_id: str = Column(String(), default=None, nullable=True, index=True)
    name: str = Column(String(), default=None, nullable=True, index=True)

    def __repr__(self):
        return f"<{type(self).__name__} {self.mnx_id}>"


class CompoundSearchSchema(SchemaClass):
    """Define the search index schema."""

    index = STORED
    compound_id = STORED
    name = NGRAM(stored=False, minsize=2, maxsize=4)


def init_db(session):
    logger.info("Adding compounds.")
    session.add(Compound(mnx_id="MNXM504217", name="1-Chloropentane"))
    session.add(
        Compound(mnx_id="MNXM39260", name="S-Aminomethyldihydro-lipoate")
    )
    session.commit()


def index(session, index_location):
    try:
        logger.info("Removing any index at location %r.", index_location)
        rmtree(index_location)
    except FileNotFoundError:
        logger.debug("No previous index found.")
    logger.info("Indexing compound names.")
    os.mkdir(index_location)
    idx = search_index.create_in(index_location, CompoundSearchSchema)
    writer = idx.writer(procs=1)
    query = session.query(Compound)
    for row in query.yield_per(10000):
        writer.add_document(index=row.id, compound_id=row.mnx_id, name=row.name)
    logger.info("Finalizing index.")
    writer.commit()


def main(db_url, index_location):
    engine = create_engine(f"sqlite:///{db_url}")
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    session = Session(bind=engine)
    init_db(session)
    index(session, index_location)
    quilt.build("midnighter/democache", join("quilt", "build.yml"))
    quilt.push("midnighter/democache", is_public=True)


if __name__ == "__main__":
    logging.basicConfig(level="INFO", format="%(message)s")
    main(
        join(dirname(__file__), "quilt", "compounds.sqlite"),
        join(dirname(__file__), "quilt", "index"),
    )
